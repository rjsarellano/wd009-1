const { ApolloServer, gql } = require("apollo-server-express"); // getting apollo server from apollo library

const { GrapQLDateTime } = require("graphql-iso-date");

const Member = require("../models/Member");

const Task = require("../models/Task");

const Team = require("../models/Team");

const customScalarResolver = {
	Date: GrapQLDateTime
};

const typeDefs = gql`
	scalar Date

	type TeamType {
		id: ID!
		name: String
		createdAt: Date
		updatedAt: Date
		task: [TaskType]
		taskId: String!
	}

	type TaskType {
		id: ID!
		description: String!
		teamId: String
		createdAt: Date
		updatedAt: Date
		isCompleted: Boolean!
		team: [TeamType]
	}

	type MemberType {
		id: ID!
		firstName: String
		me: String!
		teamId: String!
		team: [TeamType]
		lastName: String!
		position: String!
		createdAt: Date
		updatedAt: Date
	}

	# the Query type is the root of all GrapQL queries
	# this is used for executing "GET" Requests

	type Query {
		#create a query called hello that will expect a string data type

		#hello: Float!

		# square brackets indicate that we are expecting an array
		getTeams(id: String): [TeamType]
		getTasks(taskId: String, description: String): [TaskType]
		getMembers(membersId: String!): [MemberType]
	}

	# CUD functionality
	# mutating the server and the database

	type Mutation {
		createTask(description: String!): TaskType
		createTeam(name: String!): TeamType
		createMember(
			firstName: String!
			lastName: String!
			position: String!
			teamId: String!
		): MemberType
		updateMember(
			id: String!
			firstName: String!
			lastName: String!
			position: String!
			teamId: String!
		): MemberType
		updateTeam(id: String!, name: String!): TeamType
		updateTask(
			id: String!
			description: String!
			teamId: String!
			isCompleted: Boolean!
		): TaskType
	}
`;

// resolvers have 4 built in parameters passed into them
//mutate(parent,args,content, info)
const resolvers = {
	Query: {
		/*hello : () => 1.5*/

		getTeams(parent, args) {
			if (args.id) {
				return Team.find({ _id: args.id });
			} else {
				return Team.find();
			}
		},

		getTasks(parent, args) {
			console.log(args.taskId);
			return Task.find({ _id: args.taskId });
		},

		getMembers(parent, args) {
			console.log(args.membersId);
			return Member.find({ _id: args.membersId });
		}
	},

	TeamType: {
		task(parent, args) {
			console.log(parent);
			return Task.find({ teamId: parent.id });
		}
	},

	TaskType: {
		team(parent, args) {
			console.log(parent);
			return Team.find({ _id: parent.teamId });
		}
	},

	Mutation: {
		createTeam: (_, args) => {
			console.log(args);
			let newTeam = Team({
				name: args.name
			});

			console.log(newTeam);
			return newTeam.save();
		},

		createTask: (_, args) => {
			console.log(args);
			let newTask = Task({
				description: args.description,
				teamId: args.teamId,
				isCompleted: args.isCompleted
			});

			console.log(newTask);
			return newTask.save();
		},

		createMember: (_, args) => {
			console.log(args);
			let newMember = Member({
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamId: args.teamId
			});

			console.log(newMember);
			return newMember.save();
		},

		updateMember: (_, args) => {
			return Member.findOneAndUpdate(
				{
					_id: args.id
				},
				{
					firstName: args.firstName,
					lastName: args.lastName,
					position: args.position
				}
			);
		},

		updateTeam: (_, args) => {
			return Team.findOneAndUpdate(
				{
					_id: args.id
				},
				{
					name: args.name
				}
			);
		},

		updateTask: (_, args) => {
			return Task.findOneAndUpdate(
				{
					_id: args.id
				},
				{
					description: args.description
				}
			);
		}
	}
};

//create an instance of the apollo server
// in the most basic sense, the apolloServer can be started
//by passsing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
