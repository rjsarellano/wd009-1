//Backend server
//declare and use the dependencies
const express = require("express");  //use the express library
const app = express(); //app is now our server
const mongoose = require("mongoose"); //use the mongoose library

//Database connection

/*mongoose.connect("mongodb://localhost:27017/b33_merng_tracker", {
	useNewUrlParser: true
})*/

mongoose.connect("mongodb+srv://rsarellano:5Da7lgkooc123$@cluster0-3h6es.mongodb.net/b33-merng?retryWrites=true&w=majority", {
	useNewUrlParser: true
})

mongoose.connection.once("open", ()=>{
	console.log("Now connected to the Online MongoDB server");
})



// import the instantiation of the apollo server

const server = require('./queries/queries.js');

//the app will be served by apollo server instead of express

server.applyMiddleware({ app });

//Server initializtion
app.listen(4000, ()=>{
	console.log(`🚀  Server ready at http://localhost:4000 ${server.graphqlPath}`);
});

