//model
const mongoose = require("mongoose");
const Schema = mongoose.Schema; //calls Schema for us to be able to create schemas

const TaskSchema = new Schema({
	description: String,
	teamId: String,
	isCompleted: Boolean 
}, {
	timestamps: true
});

module.exports = mongoose.model("Task", TaskSchema);
//export so that the other files can use the Task model